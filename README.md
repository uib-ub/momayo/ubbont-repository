# Ubbont repository

Repository containing datamodel for marcus.uib.no/katalog.skeivtarkiv.no.

ontology is main folder in use, with the ontology that is posted at https://data.ub.uib.no/ontology/ubbont.owl

This ontology is also vizualised on changes found at https://ubbdev.gitlab.io/ubbont-repository/index-en.html (mirrored and pushed on commit to master at git.app.uib.no)


